<?php

namespace Controller;


use Http\Request;
use Notes\NoteService;

class NoteListController extends BaseController
{
    public static function handle(Request $request): void
    {
        if ($request->getAction() === 'new' && $request->getUser()) {
            NoteService::createNewNote($request->getUser());
        } elseif ($request->getAction() === 'update' && $request->getUser()) {
            NoteService::updateNote($request->getPost('id'), $request->getPost('content'));
        } elseif ($request->getAction() === 'delete' && $request->getUser()) {
            NoteService::deleteNote($request->getPost('id'));
        }
    }

    protected static function getTitle(): string
    {
        return 'Notes';
    }

    protected static function renderMain(Request $request): void
    {
        $user = $request->getUser();
        if (!$user) {
            echo '<div class="container py-5">
                    <h2>Notes</h2>
                    <p class="lead">You need to login to manage your notes</p>
                    <a href="/user/login" class="btn btn-primary">Login</a>
                  </div>';
            return;
        }

        echo '<section class="mt-5">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <h2>Notes</h2>
                    <p class="lead">Here are your pesonal notes.</p>
                  </div>
                </div>
            ';

        $notes = NoteService::getNotesByUser($user);
        foreach ($notes->getNotes() as $note) {
            echo '<div class="card">
                    <div class="card-body">
                        <form method="POST" action="/note/update">
                            <input name="id" type="hidden" value="'.$note->getId().'"/>
                            <textarea name="content" class="form-control">'.$note->getContent().'</textarea>
                            <button type="submit">Save</button>
                        </form>
                        <form method="POST" action="/note/delete">
                            <input name="id" type="hidden" value="'.$note->getId().'"/>
                            <button type="submit">Delete</button>
                        </form>
                      </div>
                  </div>';
        }

        echo '<div class="card">
                <div class="card-body">
                    ' . ($notes->empty() ? '<p class="lead">You have no notes yet.</p>' : '') . '
                    <a href="/note/new" class="btn btn-primary">Add new note</a>
                  </div>
              </div>
          </div>
        </section>';
    }
}