<?php

namespace Controller;


use Http\Request;
use User\UserService;

class UserController extends BaseController
{
    public static function handle(Request $request): void
    {
        if ($request->getAction() === 'register') {
            $success = UserService::createUser($request->getPost('mail'), $request->getPost('password'));
            $request->setParam('registrationStatus', $success ? 'success' : 'failed');
        } elseif ($request->getAction() === 'login' && $request->getMethod() === 'POST') {
            $success = UserService::loginUser($request->getPost('mail'), $request->getPost('password'));
            $request->setParam('loginStatus', $success ? 'success' : 'failed');
        } elseif ($request->getAction() === 'logout') {
            UserService::logout($request);
        }
    }

    protected static function getTitle(): string
    {
        return 'User';
    }

    protected static function renderMain(Request $request): void
    {
        $statusHtml = '';
        $loginStatus = $request->getParam('loginStatus');
        if ($loginStatus) {
            if ($loginStatus === 'failed') {
                $statusHtml = '<div class="alert alert-danger" role="alert">Login failed</div>';
            } elseif($loginStatus === 'success') {
                echo '<div class="container py-5">
                            <div class="alert alert-success" role="alert">Login successful</div>
                            <a href="/" class="btn btn-primary">Go to Notes</a>
                      </div>';
                return;
            }
        }

        $registrationStatus = $request->getParam('registrationStatus');
        if ($registrationStatus === 'failed') {
            $statusHtml = '<div class="alert alert-danger" role="alert">Registration failed</div>';
        } elseif($registrationStatus === 'success') {
            $statusHtml = '<div class="alert alert-success" role="alert">Registration successful</div>';
        }

        if ($request->getUser()) {
            echo '<div class="container py-5">
                        <a href="/user/logout" class="btn btn-primary">Logout</a>
                  </div>';
            return;
        }

        echo '<div class="container py-5">
                    '.$statusHtml.'
                <div class="row">
                    <div class="col-md-6">
                        <!-- Login Form -->
                        <h3>Login</h3>
                        <form method="POST" action="/user/login">
                            <div class="form-group">
                                <label for="loginEmail">Email address</label>
                                <input type="email" class="form-control" name="mail" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="loginPassword">Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <button type="submit" class="btn btn-primary">Login</button>
                        </form>
                    </div>
                    <div class="col-md-6">
                        <!-- Register Form -->
                        <h3>Register</h3>
                        <form method="POST" action="/user/register">
                            <div class="form-group">
                                <label for="registerEmail">Email address</label>
                                <input type="email" class="form-control" name="mail" aria-describedby="emailHelp">
                            </div>
                            <div class="form-group">
                                <label for="registerPassword">Password</label>
                                <input type="password" class="form-control" name="password">
                            </div>
                            <button type="submit" class="btn btn-primary">Register</button>
                        </form>
                    </div>
                </div>
            </div>';
    }
}