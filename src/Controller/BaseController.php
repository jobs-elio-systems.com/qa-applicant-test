<?php

namespace Controller;

use Http\Request;
use Notes\NoteService;

abstract class BaseController
{
    /**
     * Use this function to process post, etc. requests
     * @param Request $request
     * @return void
     */
    public static function handle(Request $request): void
    {

    }

    public static function render(Request $request): void
    {
        echo '<html>';
        static::renderHead();
        static::renderBody($request);
        echo '<html>';
    }

    protected static function renderHead(): void
    {
        echo '<head>
                <title>'.static::getTitle().'</title>
                <link rel="stylesheet" href="./../ressources/bootstrap-4.3.1-dist/css/bootstrap.css">
            </head>';
    }

    protected static function getTitle(): string
    {
        return '';
    }

    protected static function renderBody(Request $request): void
    {
        echo '<body>';
        static::renderNav();
        static::renderMain($request);
        echo '</body>';
    }

    protected static function renderNav(): void
    {
        echo '<nav class="navbar navbar-expand-lg navbar-light bg-light">
                <a class="navbar-brand" href="#">Notetaker</a>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item '.(static::class === NoteListController::class ? 'active' : '').'">
                            <a class="nav-link" href="/">MyNotes '.(static::class === NoteListController::class ? '<span class="sr-only">(current)</span>' : '').'</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link '.(static::class === UserController::class ? 'active' : '').'" href="/user/login">Login '.(static::class === UserController::class ? '<span class="sr-only">(current)</span>' : '').'</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link '.(static::class === AboutController::class ? 'active' : '').'" href="/about">About '.(static::class === AboutController::class ? '<span class="sr-only">(current)</span>' : '').'</a>
                        </li>
                    </ul>
                </div>
            </nav>';
    }

    protected static function renderMain(Request $request): void
    {
        echo '<main>Opps no content</main>';
    }
}