<?php

namespace Controller;

use Http\Request;

class PageNotFoundController extends BaseController
{
    protected static function getTitle(): string
    {
        return '404 Not Found';
    }

    protected static function renderMain(Request $request): void
    {
        echo '<div class="d-flex align-items-center justify-content-center" style="height: 30vh;">
                <div class="text-center">
                    <img src="/ressources/img/404.png" alt="404 Image" class="img-fluid pb-3">
                    <h1 class="display-1">404</h1>
                    <p class="lead">Page not found</p>
                    <p class="lead">Just like socks in a washing machine, this page couldn\'t be found!</p>
                    <a href="/" class="btn btn-primary">Take me Home</a>
                </div>
            </div>';
    }
}