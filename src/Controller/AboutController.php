<?php

namespace Controller;

use Http\Request;

class AboutController extends BaseController
{
    protected static function getTitle(): string
    {
        return 'About';
    }

    protected static function renderMain(Request $request): void
    {
        echo '<section class="mt-5">
              <div class="container">
                <div class="row">
                  <div class="col-lg-8 mx-auto">
                    <h2>About Note Taker</h2>
                    <p class="lead">This is a text application created with plain php and contains several issues. The target is to find all issues and recommend solutions.</p>
                    <p>This application is not using any framework to keep it simple and to avoid knowledge about them.</p>
                  </div>
                </div>
              </div>
            </section>';
    }
}