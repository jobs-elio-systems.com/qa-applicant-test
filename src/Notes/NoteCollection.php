<?php

namespace Notes;

class NoteCollection
{
    private array $notes;

    public function __construct(array $notes = [])
    {
        $this->notes = $notes;
    }

    public function getNotes(): array
    {
        return $this->notes;
    }

    public function addNote(Note $note): void
    {
        $this->notes[] = $note;
    }

    public function removeNoteById(int $id): void
    {
        foreach ($this->notes as $key => $note) {
            if ($note->getId() === $id) {
                unset($this->notes[$key]);
            }
        }
    }

    public function empty(): bool
    {
        return count($this->notes) === 0;
    }
}