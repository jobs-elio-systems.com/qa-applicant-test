<?php

namespace Notes;


use Db\DbConnection;
use User\User;

class NoteService
{
    public static function getNotesByUser(User $user): NoteCollection
    {
        $sql = 'SELECT * FROM note where user_id = :userId';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->bindValue(':userId', $user->getId());
        $stmt->execute();
        $rows = $stmt->fetchAll();

        $notes = new NoteCollection();
        foreach ($rows as $row) {
            $notes->addNote(new Note($row['id'], $row['content']));
        }

        return $notes;
    }

    public static function createNewNote(User $user): true
    {
        $sql = 'INSERT INTO note (user_id, content) VALUES (:userId, "")';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->bindValue(':userId', $user->getId());
        $stmt->execute();
        return true;
    }

    public static function updateNote(int $id, string $content): true
    {
        $sql = 'UPDATE note SET content = :content WHERE id = :id';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->bindValue(':content', $content);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return true;
    }

    public static function deleteNote(int $id): bool
    {
        $sql = 'DELETE FROM note WHERE id = :id';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->bindValue(':id', $id);
        $stmt->execute();
        return true;
    }
}