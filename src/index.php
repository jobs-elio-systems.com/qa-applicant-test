<?php

use Controller\AboutController;
use Controller\NoteListController;
use Controller\PageNotFoundController;
use Controller\UserController;
use Http\Request;
use User\UserService;

require_once __DIR__.'/autoloader.php';

# this is the entry point for all incomming requests. Depending on the path we call the right controller
$uri = $_SERVER['REQUEST_URI'];
$uri = strtolower($uri);
$uri = ltrim($uri, '/');
$controller = explode('/', $uri)[0];
$action = explode('/', $uri)[1] ?? '';

$request = new Request($controller, $action, $_SERVER['REQUEST_METHOD']);
$request->setUser(UserService::getLoggedInUser());

if ($request->getController() == '' || $request->getController() === 'note') {
    NoteListController::handle($request);
    NoteListController::render($request);
} elseif ($request->getController() === 'about') {
    AboutController::render($request);
} elseif ($request->getController() === 'user') {
    UserController::handle($request);
    UserController::render($request);
} else {
    PageNotFoundController::render($request);
}