<?php

namespace User;


use Db\DbConnection;
use Http\Request;

class UserService
{
    public static function createUser(string $mail, string $password): bool
    {
        $sql = 'SELECT TRUE FROM user WHERE email = "'.$mail.'"';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->execute();
        $exists = $stmt->fetchColumn();

        if ($exists) {
            return false;
        }

        $password = password_hash($password, PASSWORD_DEFAULT);
        $sql = 'INSERT INTO user (email, password) VALUES (:email, :password)';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->bindParam(':email', $mail);
        $stmt->bindParam(':password', $password);
        $stmt->execute();
        return true;
    }

    public static function loginUser(string $mail, string $password): bool
    {
        $sql = 'SELECT password FROM user WHERE email = "'.$mail.'"';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->execute();
        $existingPassword = $stmt->fetchColumn();

        if (password_verify($password, $existingPassword)) {
            $id = session_id();
            $sql = 'UPDATE user SET session_id = :sessionId WHERE email = :email';
            $stmt = DbConnection::getDb()->prepare($sql);
            $stmt->bindParam(':sessionId', $id);
            $stmt->bindParam(':email', $mail);
            $stmt->execute();
            return true;
        }

        return false;
    }

    public static function getLoggedInUser(): ?User
    {
        session_start();
        $id = $_COOKIE['PHPSESSID'];
        $sql = 'SELECT * FROM user WHERE session_id = "'.$id.'"';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->execute();
        $rows = $stmt->fetchAll();
        $row = array_shift($rows);

        if (!$row) {
            return null;
        }

        return new User($row['id'], $row['email']);
    }

    public static function logout(Request $request): void
    {
        $id = session_id();
        $sql = 'UPDATE user SET session_id = NULL WHERE session_id = "'.$id.'"';
        $stmt = DbConnection::getDb()->prepare($sql);
        $stmt->execute();
        session_destroy();
        $request->setUser(null);
    }
}