<?php

namespace Db;


use PDO;

class DbConnection
{
    public static function getDb(): PDO
    {
        $config = include __DIR__.'/../config.php';
        $dbConfig = $config['db'];
        return new PDO('mysql:host='.$dbConfig['host'].';dbname='.$dbConfig['dbname'], $dbConfig['username'], $dbConfig['password']);
    }
}