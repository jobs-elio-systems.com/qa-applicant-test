<?php

namespace Http;

use User\User;

class Request
{
    private string $controller;
    private string $action;
    private string $method;
    private array $params;
    private ?User $user = null;

    public function __construct(string $controller, string $action, string $method, array $params = [])
    {
        $this->controller = $controller;
        $this->action = $action;
        $this->method = $method;
        $this->params = $params;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function setController(string $controller): void
    {
        $this->controller = $controller;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): void
    {
        $this->action = $action;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): void
    {
        $this->method = $method;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): void
    {
        $this->params = $params;
    }

    public function setParam(string $name, mixed $value): void
    {
        $this->params[$name] = $value;
    }

    public function getParam(string $name): mixed
    {
        return $this->params[$name] ?? null;
    }

    public function getPost(string $name): ?string
    {
        $value = $_POST[$name] ?? null;
        return empty($value) ? null : $value;
    }

    public function setUser(?User $user): void
    {
        $this->user = $user;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }
}