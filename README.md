# Applicant Test

This is a test project used for qa applicants to demonstrate their skills. The project is a simple PHP application that
connects to a MySQL database. The project is dockerized and can be run using Docker Compose.

## Pre-requisites

Make sure you have Docker and Docker Compose installed in your system. If not, you can download them from the [official Docker website](https://www.docker.com/products/docker-desktop).

## Setup and Running

1. Clone the project: `git clone <your-repository-link>`

2. Navigate into the project directory: `cd <YourProjectDir>`

3. Run Docker Compose: `docker-compose up -d`

4. Open phpMyAdmin `http://localhost:8181/` and login with username "note" and password "note". Select the database `notes` and import the database that you can find in the file [notes.sql](notes.sql)

5. At this point, you should be able to visit your PHP application hosted by Apache at `http://localhost:8080`.

## Todos

Our target is to identify potential bugs, security issues and code quality issues. If you identify any other problem, you
can include that in your report. You should analyze and understand the application create a plan and execute the tests.
You should provide a report with your findings and recommendations.
Furthermore you should implement a few tests that can be automated to demonstrate your skills. You can choose what kind 
of automated tests you want to implement. You can use any testing framework you want.

- [ ] Run the application locally
- [ ] Understand the application and its functionalities
- [ ] Decide which dynamic and static measures you want to perform, execute them and write a report with your findings and recommendations
- [ ] Create automated tests (one kind of test is enough, e.g. unit tests, component tests, end-to-end tests, etc.)
- [ ] Write a summary how you approached this application, why you chose the tests you implemented and how you executed them
- [ ] Submit your results via mail -> see section below

Note: Using automated tools, scanners and more is great, and you should use and mention them in your result, but consider that we are interested in your skills and not the skills of a tool.

### Submit your results
- You should submit a short summary how you approached this application, how you planned your tests and how you executed them
- You should submit your findings report
- You should submit your automated tests and instructions on how to run them. It is up to you if you zip the tests or provide a link to a repository that we can access.
- You should submit any other document (plans, ...) created during the test
- Your result should be submitted to **jobs@elio-systems.com**

### Files and directories that can be ignored

The following files and directories can be ignored. You do not need to include them in your findings report.

- README.md
- Dockerfile
- docker-compose.yml
- src/.htaccess
- src/autoloader.php
- src/ressources/bootstrap-4.3.1-dist/*

### Known Issues

The following issues are know and exist by design. You are not required to add the following issues into your findings report.

- The binding between route and controller is hardcoded in our index.php. This means that any new controller must be registered there.
- No CSRF tokens or alternative security measures are implemented.
- No MFA or 2FA is implemented.
- No delete account is implemented.
- The database credentials are hardcoded in the PHP application.
- The database connection is not handled properly. If the database is down, the application will fail.
- The application is not using any framework or library to handle the database connection.
- No secure connection while accessing the application locally.
- Autoloader and namespaces
- Userinterface design does not need to be checked (usability, accessibility, ui design, etc.)